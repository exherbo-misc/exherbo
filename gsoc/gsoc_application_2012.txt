* Organization id*
Exherbo


* Organization name*
Exherbo Linux


* Organization description*
Exherbo Linux is a source-based Linux distribution written from scratch.

It was announced May 19th 2008 and caused quite a bit of waves in the
hours after the announcement hitting big news sites like Slashdot and
The Register.

Since the announcement we've made great strides towards providing a
strong base for a flexible distribution based on a distributed
development model. This model allows users to contribute freely to the
project on terms very near those official Exherbo developers work
under and today we have contributions from more users than developers
proving our community model.

We have a strong emphasis on

- up-front configurations, meaning our users know in advance what
  they're going to get from common system administration tasks like
  package installations and updates,

- decentralised development done properly, e. g. we strongly encourage
  all our users to participate actively in advancing the distribution
  as a whole - in terms of technical advancement, documentation and
  other metadata and last but not least, good ideas. No contribution
  is too small, none too great if only it's done in a technically
  sound way.

- making getting into Exherbo easy; we have mailinglists, active
  channels on Freenode, a bugtracker and other resources both provided
  by ourselves as well as resources run by users on their own but with
  our support. We enable and empower people to become active as a
  valuable part of our community.


* Organization home page url*
http://exherbo.org


* Main organization license*
GPLv2


* Backup Admin*
kloeri


* What is the URL for your Ideas page?*
http://www.exherbo.org/docs/project-ideas.html


* What is the main IRC channel for your organization?*
#exherbo  on irc.freenode.net


* What is the main development mailing list for your organization?*
http://lists.exherbo.org/mailman/listinfo/exherbo-dev


* Why is your organization applying to participate in Google Summer of
  Code 2012? What do you hope to gain by participating?*

Exherbo is strongly based on a distributed development model where we
try to involve users as much as possible in the development. In fact,
we like to say that we actually have no users but developers only
because most of our regulars start contributing to Exherbo pretty
quickly.

We're already fairly successful in getting contributions in form of
new or updated packages and new or updated Exherbo documentation.
Many important parts of our website have been written almost entirely
by users (like our installation guide, FAQ and big parts of the more
technical documentation as well, as can be seen in their respective
copyright notices).

In fact, we've codified our expectations from our non-core developers:

"What we expect from developers (that means you)

First of all you need to understand that using Exherbo is an active
commitment. We fully expect you to help with development or other
parts of the project. In return you get to have lots of fun, learn
some new things and will generally have a much easier time getting
help as you already know the workflow in Exherbo.

Secondly you’re expected to read relevant documentation including
everything on Exherbo and relevant man pages when running into
problems. Pay attention to news items and the exherbo-dev mailing
list. If you can’t solve your problem by reading documentation we’ll
be happy to help but we might yell at you if you skip this crucial
step.

We also expect you to listen to other people when asking for help and
do exactly what they ask. When we ask for a full buildlog we really
want the full buildlog and not just the last 50 lines before the error
message. You can save everybody a lot of pain if you follow other
people’s advice exactly.

You’re to think for yourself. Exherbo is not a system where we
document everything so you’ll need a basic understanding of how Linux
systems work to be able to set up many things and work out problems
efficiently when necessary.

Use IRC. Our primary means of communication is through the #exherbo
channel on freenode and it tends to be much harder working with the
community outside IRC. You don’t have to be online constantly but
please try to stay around for an hour or two. We’ll get to know each
other a little better, which makes it easier to work together in the
future whether you need some bug fixed or want to contribute something
to our main package repositories or our website.

Don’t be afraid to speak up about problems. Exherbo is supposed to be
a fun project so it’s important that problems are solved quickly
before turning into something bigger. Other contributors are generally
happy to help with any problem they can as long as you make an effort
yourself.

Give back to the community. The community is giving you a lot more
than just the bits and bytes making up packages. All the help you get,
the things you learn from Exherbo and the constant improvements are
due to the community. A good way of showing your appreciation is
simply by trying to understand the rules governing the community and
trying to follow as best you can."

We strongly believe that Google Summer of Code matches this very well
and is a good chance for students to take on larger and more important
work in the project. This is something we'd like to improve at because
albeit having great contributions from our users, we'd love to see
people working on some of larger ideas we have and that need intensive
involvement that GSoC would most likely help greatly with.

Furthermore, we'd like to attract "new blood" with attractive new
projects to participate in and, undoubtedly, the exposure we'd get by
particpating in GSoC would help that immensely. Since we're fairly
good at working with people from all around the globe and usually
successfully bond with them so that they become lasting members (and
often core-developers) of our community.


* Did your organization participate in past Google Summer of Codes? If
  so, please summarize your involvement and the successes and
  challenges of your participation.*
No, unfortunately, we weren't accepted but, last year, were
"encourage[d] [...] to try again next year." - so here we are
again. :-)

So, our biggest challenge has turned out to be accepted in the first
place. In case we don't make it this year, we'd be really grateful to
learn about the reasons as last year's kind feedback from Carol Smith
helped us prepare this year even more thoroughly - and I'm sure this
year's success will prove us right! ;-)


* If your organization has not previously participated in Google
  Summer of Code, have you applied in the past? If so, for what
  year(s)?*
We applied as a mentoring organisation for the last three years but
unfortunately didn't make it. We're hoping to make it this year as we
have lots of interesting projects and a strong interest in seeing new
developers contribute to Exherbo.

Several of our developers have worked as administrators, mentors
and/or students in previous years as members of other open source
projects.


* Does your organization have an application template you would like
  to see students use? If so, please provide it now.*
Name:
email address:
IRC nick (on Freenode):
Alternative means of contact (only for use in case of communication problems):
GPG public key ID (optional):
SSH public key (optional):
Prior experience in/contributions to F/OSS communities/projects (optional):


* What criteria did you use to select your mentors for this year's
  program? Please be as specific as possible.*
Mentors are primarily chosen based on knowledge related to the student
project, of course. Every suggested mentor has extensive knowledge
about the the project idea he's list for on our ideas page and has
been involved for a long time with Exherbo.

Secondly, to be effective, a mentor must not only be knowledgeable and
experienced, he needs to be personally interested in an idea. Things
simply won't turn out right without that.

Thirdly, availability (both time in general but also timezone) as well
as prior experience with Google Summer of Code were taken into account
as well.

Several of our developers have experience from previous years as
administrators, mentors or students from other projects.


* What is your plan for dealing with disappearing students?*
First of all, we hope to attract two students for each project because
in our experience, it's extremely beneficial for a project to be done
with a partner to achieve the best results.

If a student disappears from IRC (which we'd expect people to use) and
won't react to email, I'd talk to his mentor first to find out if
there have been any prior problems of either technical or personal
nature before.

We'd then try to use the alternative means of contact we're asking for
so that we can make sure the student doesn't simply have some
technical problems which we might try to help him/her with. In case of
personal issues, we should be able to assign another mentor if the
need really arises.

If we can't reach a student by any means and really have to give up on
him/her, we hope that, if there's a second student on the same project
he/she will be able to continue the work.


* What is your plan for dealing with disappearing mentors?*
While our mentors are dedicated core-developers of Exherbo and looking
forward to working with students in GSoC (again), it's of course
always possible to be hit by the proverbial truck one day.

Since we're a relatively small group of about 24 core-developers
(albeit having a large crowd of contributors in our community), we
always try to have more than one developer on each project we
undertake. Such "redundancy" has always been vital for the success of
our project as a whole.

The same principle applies to mentors: We're aiming to have "backup
mentors" for each idea and, of course, being a closely-knit group
we're all pretty much acquainted with what we want to achieve together
with our students and, thus, should be able to deal with the case of
the disappearing mentor.

Furthermore, we, too, apply the principle of always having backup
communication means so that we can stay in touch even if someone
should "drop off the net".

(I've personally received concerned text messages and voice calls when
I was buried in "real life" work last year so the system works (and as
a beneficial side effect makes one feel appreciated). :-) )


* What steps will you take to encourage students to interact with your
  project's community before, during and after the program?*
We're a very open and embracing community (with clear expectations,
though, as stated before) and we try to get people interested in
improving small things in our project and try to carefully explore
their further interests. 
It's a bad idea to just dump a load of work in front of someone and
ask him to figure it out. Instead we encourage people to try Exherbo
out, to interact with us to see if we're mutually "compatible" to
avoid disappointment and false impressions from the start.

In our experience, there's hardly anything for a newcomer that
compares to seeing his/her patch (no matter how small) being accepted
and pushed and duly credited to its author. Sometimes it may seem like
hard work because we have clearly stated quality guidelines
(http://exherbo.org/docs/contributing.html) but it's only the more
rewarding (and helps *us* evaluating the contributor's skills) when
success is achieved - and we ("we" being potentially every single
member of our community) make sure it is.

This way of working has worked extremely well for us so far and we
think we can keep this up for GSoC if we're accepted and might, in
turn, get some valuable feedback that will allow us to improve
further.  "Working extremely well" is reflected by the fact that most
contributors stay with us (our top 50 contributors have >=75 commits
to our git repositories and the top 100 have at least six each) for
much longer than just one summer. :-)

Thus, we think that GSoC students, too, will be attracted to our open,
decentralised and flexible development model, our strive for high
quality and improvement as well as our dedication to our common goals
will bind then-former students to our project even after the formal
program is finished.


* Are you a new organization who has a Googler or other organization
  to vouch for you? If so, please list their name(s) here.
I guess Exherbo, even though having been around since 2008 (or,
actually, 2007 but unannounced) and having well beyond 100
contributors (177, according to ohloh.net), might still count as a
small and new project. Thus, Alec "antarus" Warner, a Googler, kindly
agreed to vouch for us.

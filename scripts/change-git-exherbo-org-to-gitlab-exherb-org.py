#! /usr/bin/env python
#
# Small script to change sync URLs of Exherbo repositories
#
# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

import argparse
import difflib
from pathlib import Path
import re

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--dryrun", action='store_true',
                    help="Don't actual change config files, just display a diff of changes")
parser.add_argument("-r", "--repoconfigdir", default='/etc/paludis/repositories',
                    help="Path to the repository config files, defaults to /etc/paludis/repositories")
arguments = parser.parse_args()

dry_run = arguments.dryrun
repo_config_dir = arguments.repoconfigdir

def diff(a, b, filename):
    for line in difflib.unified_diff(a.splitlines(), b.splitlines(),
                                     fromfile=filename, tofile=filename, lineterm=''):
        print(line)

if dry_run:
    print("Displaying changes that would be done without --dryrun:")
else:
    print(f"Adjusting repository configs in {repo_config_dir}...")

dev_regex = re.compile('git://git.exherbo.org/dev/')
dev_regex_http = re.compile(r'git\+(http|https)://git.exherbo.org/(git/)?dev/')
misc_regex = re.compile(r'git://git.exherbo.org/(graveyard|unwritten)')
misc_regex_http = re.compile(r'git\+(http|https)://git.exherbo.org/(git/)?(graveyard|unwritten)')
exherbo_regex = re.compile('git://git.exherbo.org/')
exherbo_regex_http = re.compile('git\+(http|https)://git.exherbo.org/(git/)?')

count = 0

filelist = Path(repo_config_dir).glob('*.conf')
for filepath in filelist:
    with open(filepath) as file:
        fileContent = file.read()

    result = dev_regex.subn('git+https://gitlab.exherbo.org/exherbo-devs/', fileContent)
    count += result[1]
    result = dev_regex_http.subn('git+https://gitlab.exherbo.org/exherbo-devs/', result[0])
    count += result[1]

    result = misc_regex.subn(r'git+https://gitlab.exherbo.org/exherbo-misc/\1', result[0])
    count += result[1]
    result = misc_regex_http.subn(r'git+https://gitlab.exherbo.org/exherbo-misc/\3', result[0])
    count += result[1]

    result = exherbo_regex.subn('git+https://gitlab.exherbo.org/exherbo/', result[0])
    count += result[1]
    result = exherbo_regex_http.subn('git+https://gitlab.exherbo.org/exherbo/', result[0])
    count += result[1]

    if dry_run:
        diff(fileContent, result[0], str(filepath))
    else:
        fileContent = result[0]
        with open(filepath, 'w') as file:
            file.write(fileContent)

if not dry_run:
    print(f"Done. Replaced {count} matches in {repo_config_dir}")
